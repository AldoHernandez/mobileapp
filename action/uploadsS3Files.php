<?php 

  session_start();

  include_once "../config/config.php";
  require '../composer/vendor/autoload.php';
  include_once "encript.php";
  include_once "../sendEmail.php";

  use Aws\S3\S3Client;
  use Aws\Exception\AwsException;

  $datos = $con->query("SELECT * FROM tbl_keys");
  $d = $datos->fetch_all(MYSQLI_ASSOC);

  $key = $d[count($d)-1]['KEY_'];
  $secrectkey = $d[count($d)-1]['SECRETKEY'];


  $sql = "SELECT * FROM TBL_LISTDIST;";
  $query = mysqli_query($con,$sql);
  $users = mysqli_fetch_all($query, MYSQLI_ASSOC);

  function compress_image($source_url, $destination_url, $quality)
  {
      $info = getimagesize($source_url);
      if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url);
      elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url);
      elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);
      imagejpeg($image, $destination_url, $quality);
      echo "Image uploaded successfully.";
  }


  function count_comprobantes($files, $g_name){

    $num_comprobantes =  count($files) - 2;

    if(!empty($files['num_tarjetas'])){  
      $num_comprobantes = $num_comprobantes - intval($files['num_tarjetas']);
    }
  
    return $num_comprobantes;
  }

  $S3_Options = [
    'version' => 'latest',
    'region' => 'us-west-2',
    'credentials' => [

      'key' => encrypt_decrypt('decrypt', $key),
      'secret' => encrypt_decrypt('decrypt', $secrectkey)
    ]
  ];



  $carpeta = '../data';
  if (!file_exists($carpeta)) {
    mkdir($carpeta, 0777, true);
  }

  $carpeta = '../uploads';
  if (!file_exists($carpeta)) {
    mkdir($carpeta, 0777, true);
  }
  
  $S3 = new S3Client($S3_Options);

  $folio = $_POST['folio'];
  $nombre_cliente = $_POST['nombre'];
  $placa = $_POST['placa'];
  $estado = $_POST['estado'];
  $tramite = $_POST['tramite'];

  $usuario = $_SESSION['nombre'];
  $comprobantes = array();
  $tarjetas = array();

  $generic_name = $placa.'_'.$folio.'_'.str_replace(' ', '', $nombre_cliente).'_';
  //echo $generic_name;

  $path = $_FILES[$generic_name.'TRAMITE']['name'];
  $evidencia_ext = pathinfo($path, PATHINFO_EXTENSION);
  $e_tramite = $generic_name.'TRAMITE'.'.'.$evidencia_ext;
  $_FILES[$generic_name.'TRAMITE']['name'] = $e_tramite;

  if($_POST['num_tarjetas'] > 0){
    
    for ($i=0; $i < $_POST['num_tarjetas']; $i++) { 
    
      $path = $_FILES[$generic_name.'TARJETA'.strval($i+1)]['name'];
      $tarjeta_ext = pathinfo($path, PATHINFO_EXTENSION);
      $tarjeta  = $generic_name.'TARJETA'.strval($i+1).'.'.$tarjeta_ext;
      array_push($tarjetas, $tarjeta);
      $_FILES[$generic_name.'TARJETA'.strval($i+1)]['name'] = $tarjeta;
    }

    $tarjetas =  implode("|", $tarjetas);


  }else{
    $tarjetas = "";
  }


  $path = $_FILES[$generic_name.'ACUSE']['name'];
  $acuse_ext = pathinfo($path, PATHINFO_EXTENSION);
  $acuse = $generic_name.'ACUSE'.'.'.$acuse_ext;
  $_FILES[$generic_name.'ACUSE']['name'] = $acuse;


  if($_POST['num_comprobantes'] > 0){

    
    for ($i=0; $i < $_POST['num_comprobantes']; $i++) { 
    
      $path = $_FILES[$generic_name.'COMPROBANTE'.strval($i+1)]['name'];
      $comprobante_ext = pathinfo($path, PATHINFO_EXTENSION);
      $comprobante  = $generic_name.'COMPROBANTE'.strval($i+1).'.'.$comprobante_ext;
      array_push($comprobantes, $comprobante);
      $_FILES[$generic_name.'COMPROBANTE'.strval($i+1)]['name'] = $comprobante;
    }

    $comprobantes =  implode("|", $comprobantes);


  }else{
    $comprobantes = "";
  }


  foreach ($_FILES as $file ) {
      compress_image($file['tmp_name'], "../data/".$file['name'], 50);
  }
  
  $sql = "INSERT INTO TBL_ENTREGAS(LED, PLACA, NOMBRE_CLIENTE, ESTADO, TRAMITE, ID_EVIDENCIA, ID_COMPROBANTE, ID_ACUSE,
    ID_TARJETA, USUARIO) VALUES('$folio','$placa', '$nombre_cliente', '$estado', '$tramite', 
    '$e_tramite', '$comprobantes', '$acuse', '$tarjetas','$usuario')";


  $con->query($sql);

  foreach($_FILES as $file){

    $uploadObject = $S3->putObject([
        'Bucket' => 'jj-gest-prod-nexu',
        'Key' => '../data/'.$file['name'],
        'SourceFile' => $file['tmp_name'] 
    ]);
    
  }
  
    
  if(sent_email($placa,$folio,$tramite,$nombre_cliente,$users)){
    $dir = '../data/';
    foreach(glob($dir.'*.*') as $v){
      unlink($v);
    }
  }

?>