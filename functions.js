    const all_files = [];
    const tarjetas = [];

      function validate_data(){

  
        let valid = true;
        $('[required]').each(function() {
          if ($(this).is(':invalid') || !$(this).val()) valid = false;
        })
        if (!valid)
          (function () {
            'use strict'

            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.querySelectorAll('.needs-validation')

            // Loop over them and prevent submission
            Array.prototype.slice.call(forms)
              .forEach(function (form) {

                  if (!form.checkValidity()) {

                    event.preventDefault()
                    event.stopPropagation()

                  }else{
                    document.getElementById("loader").style.display = "block";  
                  }  

                  form.classList.add('was-validated')
                
              })
          })()
        else{ 
         
            let folio = $("#floatingInputFolio").val();       
            let nombre = $("#floatingInputNombre").val();
            console.log(nombre);
            let placa = $("#floatingInputPlaca").val();

            let estado = $("#estado").val();
            let tramite = $("#tramite").val();

            let e_tramite = $('#exampleInputEvidencia').prop('files');
            let acuse = $('#exampleInputAcuse').prop('files');
            
            let comprobantes = $('#exampleInputComprobante')[0].files;
            let tarjeta = $('#exampleInputTarjeta')[0].files;

            let archivos = new FormData();

            nombreSplit = nombre.replace(" ", "");
            let genericName = placa+'_'+folio+'_'+nombreSplit;
            genericName = genericName.trim();

            console.log(nombreSplit);
            console.log(genericName);
            console.log(genericName);
            console.log(genericName);


            for (let i = 0; i < all_files.length; i++) {
              archivos.append(genericName+'_COMPROBANTE'+(i+1), all_files[i]);
            }

            for (let i = 0; i < tarjetas.length; i++) {
              archivos.append(genericName+'_TARJETA'+(i+1), tarjetas[i]);
            }

            archivos.append(genericName+'_TRAMITE' , e_tramite[0]);
            archivos.append(genericName+'_ACUSE' , acuse[0]);


            archivos.append('folio',folio);
            archivos.append('nombre',nombre);
            archivos.append('placa',placa);
            archivos.append('estado',estado);
            archivos.append('tramite',tramite);
            archivos.append('num_tarjetas', tarjetas.length);
            archivos.append('num_comprobantes',all_files.length);

            Swal.fire('Cargando Datos');
            Swal.showLoading(); 
           
            $.ajax({
              url: 'action/uploadsS3Files.php',
              type: 'post',
              data: archivos,
              contentType: false,
              processData: false,
              success: function(response) {

                console.log(response);

                  nombreSplit = "";
                  genericName = "";

                 Swal.hideLoading();
                 Swal.fire({
                    title: 'Tramite completo',
                    text: "Los datos se han cargado correctamente",
                    icon: 'success',
                    showConfirmButton: false,
                    timer: 1500
                  }).then((value) => {
                    location.reload();
                  });
              }
            }); 

        }
      }


      function delete_item(id){
        all_files.splice(id, 1);

        console.log(all_files)

        $( "#items_files" ).empty();

        show_items();
        console.log($('#exampleInputComprobante').prop('files'));
     
      }


      function delete_item_tarjeta(id){
        tarjetas.splice(id, 1);

        console.log(tarjetas)

        $( "#items_tarjetas" ).empty();

        show_items_tarjeta();
        console.log($('#exampleInputTarjeta').prop('files'));
     
      }


      function show_items(){

        for (let i = 0; i < all_files.length; i++) {
          $("#items_files").append(
            `
              <div class="alert alert-info row justify-content-between p-1" role="alert">
                <div class="col-10">${all_files[i]['name']}</div>
                <div class="col-2"><button type="button" class="btn btn-danger" onclick="delete_item(${i})" >X</button></div>
              </div>
            `
          );
        }

      }

      function show_items_tarjeta(){

        for (let i = 0; i < tarjetas.length; i++) {
          $("#items_tarjetas").append(
            `
              <div class="alert alert-info row justify-content-between p-1" role="alert">
                <div class="col-10">${tarjetas[i]['name']}</div>
                <div class="col-2"><button type="button" class="btn btn-danger" onclick="delete_item_tarjeta(${i})" >X</button></div>
              </div>
            `
          );
        }

      }



      $( "#exampleInputComprobante").change(function() {

        $( "#items_files" ).empty();

        let files = $('#exampleInputComprobante').prop('files');

        console.log(files.length);

        for (let i = 0; i < files.length; i++) {

          all_files.push(files[i])
        }

        console.log("all files");
        console.log(all_files);

        show_items();
        
      });

      $( "#exampleInputTarjeta").change(function() {

        $( "#items_tarjetas" ).empty();

        let files_t = $('#exampleInputTarjeta').prop('files');

        console.log(files_t.length);

        for (let i = 0; i < files_t.length; i++) {

          tarjetas.push(files_t[i])
        }

        console.log("Tarjetas");
        console.log(tarjetas);

        show_items_tarjeta();
        
      });
