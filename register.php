<!DOCTYPE html>
<html>
<head>
    <link rel="icon" href="images/JJ.ico">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>.:Gestoria J&J:.</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="plugins/iCheck/square/blue.css">


</head>
<body class="hold-transition register-page">
    <div class="register-box">
         <div id="result"></div>
        <div class="register">
            <a href="#"><b><img width="400" height="200" src="images/logito.png"></b></a>
        </div>
        <div class="register-box-body text-center">
            <p class="login-box-msg">Nuevo Usuario</p>
            <form method="post" id="add" name="add">
                <div class="form-group has-feedback">
                    <input type="text" name="fullname" class="form-control" placeholder="Nombre y apellidos" required>
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="email" name="email" id="correo" class="form-control" placeholder="Correo Electrónico" required>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" id="pass" name="password" class="form-control" placeholder="Contraseña" required>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <p class="text-muted text-right">campos obligatorios* </p>
                <div class="row">
                
                    <button id="save_data" type="submit" class="btn btn-primary btn-block btn-flat" disabled >Registrar</button>
                   
                </div>
            </form>

            <br>
            <a href="index.php" class="text-center">Iniciar Sesión</a>

        </div><!-- /.form-box -->
    </div><!-- /.register-box -->

    <!-- jQuery 2.2.3 -->
    <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="plugins/iCheck/icheck.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>




<script>

$( "#correo" ).change(function(){


	var correo_ingresado=$("#correo").val();
    
	var subcadena=correo_ingresado.split('@',2);
    
    	var dominios=["kiatrust.com.mx","jjsolucionesempresariales.com","crfact.com.mx","testjjsolucionesempresariales.com"];
  


	if(subcadena[1]==dominios[0] || subcadena[1]==dominios[1] || subcadena[1]==dominios[2] || subcadena[1]==dominios[3]){

	//alert("Correo Valido");
    	
    $('#save_data').prop( "disabled", false );
    $('#pass').prop( "disabled", false );

    }else{
       
    	//alert("Correo Invalido");

        swal("Correo Invalido","Ingrese un correo valido","info");

        $('#pass').prop( "disabled", true );
    	
    	
    }

});



$( "#add" ).submit(function( event ) {
  $('#save_data').attr("disabled", true);
  
 var parametros = $(this).serialize();
     $.ajax({
            type: "POST",
            url: "action/adduser.php",
            data: parametros,
             beforeSend: function(objeto){
                //$("#result").html("Mensaje: Cargando...");
              },
            success: function(datos){
            //$("#result").html(datos);

            console.log(datos);

            if(datos==1){
                swal("Registro Exitoso","","success");
            }else{
                swal("Acceso denegado","Favor de ponerse en contacto con el administrador o responsable de la empresa","error");
            }

            $('#save_data').attr("disabled", false);
            //load(1);
          }
    });
  event.preventDefault();
})
</script>

</body>
</html>
