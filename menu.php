<!DOCTYPE html>
<html>
<head>
  <title>Home</title>
  <meta charset="utf-8">
  <!-- CSS only -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
  <meta name="viewport" content="width=device-width, initial-scale=1">
   <meta http-equiv="cache-control" content="max-age=0" />
  <meta http-equiv="cache-control" content="no-cache" />
  <meta http-equiv="expires" content="0" />
  <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
  <meta http-equiv="pragma" content="no-cache" />

  <style type="text/css">
    
    /* Center the loader */
    #loader {
      position: absolute;
      left: 50%;
      top: 50%;
      z-index: 1;
      width: 300px;
      height: 300px;
      margin: -75px 0 0 -75px;
      border: 16px solid #f3f3f3;
      border-radius: 50%;
      border-top: 16px solid #3498db;
      border-bottom: 16px solid #3498db;
      
      width: 120px;
      height: 120px;
      -webkit-animation: spin 2s linear infinite;
      animation: spin 2s linear infinite;

    }

        /* Safari */
    @-webkit-keyframes spin {
      0% { -webkit-transform: rotate(0deg); }
      100% { -webkit-transform: rotate(360deg); }
    }

    @keyframes spin {
      0% { transform: rotate(0deg); }
      100% { transform: rotate(360deg); }
    }
  </style>
</head>
<body  style=" height:100vh;">

  <?php session_start(); ?>

  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
      <a class="navbar-brand" href="#"><?php echo $_SESSION['nombre']; ?></a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="action/logout.php">Cerrar Sesion</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>


  <div class="container-fluid">
    <?php  include_once "config/config.php"; ?>
      <form class="m-2 needs-validation" id="myForm" enctype="multipart/form-data" novalidate >

          <div class="form-floating mb-3">
            <input type="text" class="form-control" id="floatingInputFolio" required >
            <label for="floatingInputFolio">FOLIO</label> 
          </div>

          <div class="form-floating my-3">
            <input type="text" class="form-control" id="floatingInputNombre" oninput="this.value = this.value.toUpperCase()" required>
            <label for="floatingInputNombre">NOMBRE DE CLIENTE</label>
          </div>
          <div class="form-floating mb-3">
            <input type="text" class="form-control" id="floatingInputPlaca" oninput="this.value = this.value.toUpperCase()" required>
            <label for="floatingInputPlaca" >PLACA</label>
          </div>
          <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">ESTADO EMPLACAMIENTO</label>
            <select class="form-select" aria-label="Default select example" id="estado" required>
              <option selected disabled value="" >SELECCIONE UNA OPCION</option>
              <option value="MORELOS">MORELOS</option>
              <option value="EDOMEX">EDOMEX</option>
              <option value="CDMX">CDMX</option>
              <option value="JALISCO">JALISCO</option>
              <option value="GUANAJUATO">GUANAJUATO</option>
              <option value="NUEVO LEON">NUEVO LÉON</option>
              <option value="QUERETARO">QUERETARO</option>
            </select>
          </div>
          <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">TIPO TRAMITE</label>
            <select class="form-select" aria-label="Default select example" id="tramite" required>
              <option selected disabled value="" >SELECCIONE UNA OPCION</option>
              <option value="ALTA VEHICULAR">ALTA VEHICULAR</option>
              <option value="BAJA VEHICULAR">BAJA VEHICULAR</option>
              <option value="CAMBIO PROPIETARIO">CAMBIO PROPIETARIO</option>
              <option value="TARJETA DE CIRCULACION">TARJETA DE CIRCULACION</option>
            </select>
          </div>
          <div class="mb-3">
            <label for="exampleInputEvidencia" class="form-label">EVIDENCIA DE TRAMITE</label>
            <input type="file" class="form-control" id="exampleInputEvidencia" aria-describedby="Evidencia" name="evidencia" required >
          </div>
          <div class="mb-3">
            <label for="exampleInputTarjeta" class="form-label">TARJETA CIRCULACIÓN</label>
            <input type="file" class="form-control" id="exampleInputTarjeta" aria-describedby="Tarjeta" name="tarjetas[]" multiple>
          </div>
          <div id="items_tarjetas" style="visibility: none;">
          </div>
          <div class="mb-3">
            <label for="exampleInputComprobante" class="form-label">COMPROBANTE/ CONSTANCIA DE PAGO/ TRAMITE</label>
            <input type="file" class="form-control" id="exampleInputComprobante" aria-describedby="Comprobante" name="comprobantes[]" multiple >  
          </div>
          <div id="items_files" style="visibility: none;">
          </div>

          <div class="mb-3">
            <label for="exampleInputAcuse" class="form-label">ACUSE DE RECIBIDO</label>
            <input type="file" class="form-control" id="exampleInputAcuse" aria-describedby="Acuse" name="acuse" required> 
          </div>
      </form>
      <div class="d-grid mb-2">
              <button type="submit" class="btn btn-primary" onclick="validate_data();">ENVIAR</button>
          </div>
  </div>

  <!-- JavaScript Bundle with Popper -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
  <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
  <script src="functions.js?<?php echo time(); ?>"></script>
</body>
</html>